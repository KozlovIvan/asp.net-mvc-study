﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookingAppStore.Models;

namespace BookingAppStore.Controllers
{
    public class HomeController : Controller
    {
        public void GetContext()
        {
            HttpContext.Response.Write("Htllo world!");
            string browser = HttpContext.Request.Browser.Browser;
            string user_agent = HttpContext.Request.UserAgent;
            string url = HttpContext.Request.RawUrl;
            string ip = HttpContext.Request.UserHostAddress;
            string refferer = HttpContext.Request.UrlReferrer == null ? "" : HttpContext.Request.UrlReferrer.AbsoluteUri;
            Response.Write(string.Format("<p>Browser: {0}</p><p>User-agent: {1}</p><p>Url запроса: {2}</p><p>Реферер: {3}</p><p>IP-адрес: {4}</p>",browser, user_agent, url,refferer,ip));
        }
        BookContext db = new BookContext();
        public string GetCoockeyData()
        {
            string id = HttpContext.Request.Cookies["id"].Value;
            var val = Session["name"];
            return id+" "+val;
        }
        public ActionResult Index()
        {
            //HttpContext.Response.Cookies["id"].Value = "test cookey string";
            //Session["name"] = "Tom";
            var books = db.Books;
            //ViewBag.Books = books;
            //ViewData["Head"] = "Привет мир";
            //ViewBag.Head = "Привет мир";
            //ViewBag.Fruit = new List<string>
            //{
            //    "Apple", "Pineapple", "Orange"
            //};
            return View(books);
        }
        public ActionResult BookIndex()
        {            
            var books = db.Books;           
            return View(books);
        }
        public FilePathResult GetFile()
        {
            string file_path = Server.MapPath("~/Files/Test.pdf");
            string file_type = "application/octet-stream";
            string file_name = "Test.pdf";
            return File(file_path, file_type, file_name);
        }
        public ActionResult GetVoid(int id)
        {
            if(id>3)
            {
                //return RedirectToAction("Square", "Home", new { a = 10, b = 20 });
                //return new HttpStatusCodeResult(404);
                //return HttpNotFound();
                return new HttpUnauthorizedResult();
            }
            else
            {
                return View("About");
            }
            
        }
        public ActionResult GetHtml()
        {
            return new Util.HtmlResult("<h2>Привет мир</h2>");
        }
        public ActionResult GetImage()
        {
            string path = "../Content/Images/Lindsey-Stirling-High-Definition.jpg";
            return new Util.ImageResult(path);
        }
        [HttpGet]
        public ActionResult GetBook()
        {           
            return View();
        }
        [HttpPost]
        public ContentResult PostBook()
        {
            string title = Request.Form["title"];
            string author = Request.Form["author"];
            return Content(title + " " + author);
        }
        public string GetId(int id)
        {
            return id.ToString();
        }
        public string Square()
        {
            int a = Int32.Parse(Request.Params["a"]);
            int b = Int32.Parse(Request.Params["b"]);
            double s = a * b;
            return string.Format("<h2>Площадь треугольника со сторонами {0} и {1} равна {2} </h2>",a,b,s);
        }
        [HttpGet]
        public ActionResult Buy(int id)
        {
            ViewBag.BookId = id;
            return View();
        }
        [HttpPost]
        public string Buy(Purchase purchase)
        {
            purchase.Date = DateTime.Now;
            db.Purchases.Add(purchase);
            db.SaveChanges();
            return string.Format("Спасибо, {0}, за покупку!", purchase.Person);
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "This is contact page!";
            return View();
        }
    }
}