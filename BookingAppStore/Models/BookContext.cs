﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace BookingAppStore.Models
{
    public class BookContext:DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Purchase> Purchases { get; set; }
    }

    public class BookDBInitializer: DropCreateDatabaseAlways<BookContext>
    {
        protected override void Seed(BookContext context)
        {
            context.Books.Add(new Book { Author = "Л.Н. Толстой", Name = "Война и мир", Price = 1000 });
            context.Books.Add(new Book { Author = "И.С. Тургенев", Name = "Му-му", Price = 300 });
            context.Books.Add(new Book { Author = "А.С. Пушкин", Name = "Сказки", Price = 1000 });
            base.Seed(context);
        }
    }
}